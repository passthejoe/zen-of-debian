\documentclass[12pt,a4paper]{book}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\author{Steven Rosenberg}
\title{Zen of Debian}
\begin{document}
\maketitle
\chapter{Introduction}
"The Zen of Debian" is an essay about Debian GNU/Linux. It's also a personal account of my computing journey and how an operating system like Debian — or really any Linux- or BSD-based OS — can bring order, calm, productivity and stability to your life and work.

\section*{Who am I?}

My name is Steven Rosenberg. I am a journalist, writer and very amateur programmer. Over the last decade and then some, I have written quite a bit about my experiences with computer hardware and software as a pretty much regular user learning things along the way. Much of that writing can be found at \url{http://blogs.dailynews.com/click} (mirrored at \url{https://passthejoe.wordpress.com}) and \url{http://stevenrosenberg.net/blog}. Newer posts are at \url{http://passthejoe.net}.

During years and years of blogging, I wrote hundreds of posts about Debian, Ubuntu, Fedora and other Linux distributions. I haven't counted, but I probably wrote more than 100 posts about OpenBSD.

I'm sure I've written about the drought in Linux book publishing, and I know that few are writing — and even fewer are publishing — books about how to use Linux on the desktop. There's certainly no money in it.

I began using Linux and BSD operating systems around 2007, when I didn't have a computer of my own and was hacking on old machines. I had a surplus Maxspeed Maxterm thin client that couldn't have cost more than \$30 with a VIA CPU and IDE-connected hard and CD drives hanging out the back at the end of long ribbon cables. I bought a very, very used Compaq laptop for \$15. Then someone gave me a broken Gateway laptop. I rewired the cracked-in-half  power plug it and ran it for years. Even when these castoff computers came with Microsoft Windows, I always ran Linux or BSD on them. The first distro I really used was Puppy Linux, which ran from a live CD. My first and favorite release was \href{https://distrowatch.com/?newsid=03776}{2.11\footnote{\url{https://distrowatch.com/?newsid=03776}}}.

During those early Linux days, I found out about Ubuntu during the 6.06 and 7.04 era and had heard how it was an "easier" Debian. file(First question: What's Debian, and why is it so hard?)

When Debian Etch came out (and it could have been the July 2008 release of \href{https://www.debian.org/News/2008/20080726}{Etch and a Half\footnote{\url{https://www.debian.org/News/2008/20080726}}}, I read about it, downloaded an ISO and gave it a spin. The installer wasn't much more complicated than Ubuntu's, and once I had Debian running, it looked, smelled and worked like a bluer, faster Ubuntu. At that time, Debian, Ubuntu, SuSE, Fedora and Red Hat Enterprise Linux/CentOS all shipped with a very similar GNOME 2 desktop. Package management was different (yum vs apt and all), but it soon became very apparent that if you could run Ubuntu, you could run Debian.

In more recent years — 2012 and 2017 — I have used Windows on new laptops in order to give the OS "a chance" and avoid compatibility issues that can affect new hardware. I gave Windows 10 maybe two years on my current HP Envy laptop before giving up after some very thorny upgrades and enough quirks to make me question why I didn't just return to an OS where simplicity, stability and performance are all part of the package (and package management system).

For me over the years, those OSes have been the Debian and Fedora distributions of Linux. I first used Debian in the 4.0 "Etch" era, and was surprise to find out it was a bluer, faster yet not appreciably more difficult version of what the brown yet growing Ubuntu was offering in the 6.06 era. I experimented rather heavily with both Debian and Ubuntu at the time, and I seemed to always come back to Debian.

I looked back at the list of Debian releases and realize that I've spent time using every one from Etch (Version 4) through the current stable release Buster (Version 10).

I have run a lot of Fedora over the years. I began 2010 with Version 13 and stuck with Fedora through version 15, turning to Debian Squeeze when a Fedora upgrade went really bad due to the proprietary AMD/Catalyst video driver. I returned to Fedora with my next laptop in 2012 — the Fedora 17 "Beefy Miracle" era. I still have that laptop today, and it runs Fedora 30, I had ONE bad upgrade in all those years, and that time I was able to reinstall Fedora and use the same partitions and /home directory.

With my current laptop, a 2017 HP Envy, I decided to spend some time using Windows 10, and it was a couple of years before thorny upgrades, persistent bugs and lagging performance led me to get a small, cheap M.2 NVMe SSD, and install Debian Stable (version 10 Buster). I immediately began to feel comfortable and productive on the computer I use for wrangling websites, writing and editing, hacking on Ruby, and all of the other day-to-day things I do with a PC.

\chapter{The Zen of Debian}
Installing, managing and using the Debian GNU/Linux operating system on your computers can bring a sense of calm, mastery and productivity to your life and work.

If Debian Stable works on your computer today, it will keep working for the rest of the two-year release window, plus an additional year, with minimal yet important updates that will keep your system secure.

Like most Linux operating systems — called *distributions* — Debian comes with all the tools you'll need to install and remove software using remote repositories that contain tens of thousands of packages. Those same tools will keep Debian up to date with the latest security patches and critical bug fixes.

Also with those tools, you have the power and the option to upgrade your *Stable* system to the *Testing* or Unstable* releases, or the *next* Stable release when Debian's developers deem it ready.

Whether you are a full-fledged Debian Developer, or a new user installing Debian for the first time, you are part of a history and tradition that goes back to the beginning of the Linux kernel in the 1990s, the GNU tools in the 1980s, and the first Multics and Unix operating systems of the 1960s and 1970s.

Time-sharing, an everything-is-a-file philosophy and dedication to portability were part of Unix in the 1970s, and they continue to be so today — even if you're *technically* the only user on your laptop or desktop computer. The days of Unix-running Digital Equipment Corporation VAX computers accommodating dozens of simultaneous users via "dumb" terminals over serial connections may be over, but those same dozens of users can connect to shared Unix/Linux systems via secure shell, and we know that thousands can interact with systems that connect via Web servers and are backed by databases.

\chapter{Installing Debian: How to get good}
I learned to embrace the process of installing Debian by doing it dozens and dozens of times, beginning when I first "discovered" Etch until now, when I run Buster on a 2017 HP laptop and 2011 iMac desktop. During the many years between Etch and Buster, it helped a great deal that I had many older "test" computers and hard drives at my disposal. If I screwed anything up, I could start over.

I tended to do it the same way every time: Encrypted LVM most of the time, accepting most if not all of the installer's defaults.

\section*{Non-free firmware: Do you need it? How do you get it?}

There are many ways to install Debian. That's good, but also bad. Choices bring flexibility and confusion.

In Debian — and the Debian installer — much of this confusion is due to non-free firmware.

If you don't want to use a non-free firmware image, you can also choose to keep the most common non-free firmware in a place where you can use it. On most systems, that means a FAT-formatted USB flash drive.

Debian provides a link to a compressed file of the non-free firmware in the yellow-highlighted portion of https://www.debian.org/distrib, near the bottom.

Get the .zip if you know you have the software to unzip it. In a basic Linux environment, you'll always be able to unpack a tarball, and there is a .tar.gz file for that reason. 

\chapter{Old vs. new}
The packages in Debian Stable are old. They're old when the release is new. They're even older when it's a year and a half into the release and a new Debian Stable is still 6 months away.

Even the kernel is old. The applications are definitely old. The developer tools like programming languages and IDEs (if there are any) are old.

\section*{Does any of this matter?}

There are workarounds if you must have the latest versions of a few things. And if you want the latest of everything, there are other releases and distributions that offer it.

But what if you really want those improvements? After using Linux and BSD for years, I've come to learn that latest doesn't always mean greatest, and being on the edge often brings new bugs along with it.

That's not to say that you can't run newer software in Debian. There's always the Testing and Unstable distributions, though even those I find to be behind Fedora and Arch in almost all cases.

There are some newer packages in Debian Backports. And now software packaged in the Flatpak format can run on Debian Stable. Since Buster is very IDE-poor in its repos, I have successfully used the Flathub site (where Flatpaks tend to live) to successfully install and run Apache NetBeans and IntelliJ IDEA Community. I haven't yet tried Eclipse, but Flathub has it.

Why none of these big-time Java IDEs are in Debian is another story. Eclipse and NetBeans weren't "ready" when Buster was frozen, and I think there are licensing reasons why IntelliJ is not in Debian. Flathub means you can get all three with little hassle.

\chapter{The daily update}

Repetition is bad. That's what Western culture tells us.

Repetition is mindless, it says.

But how do you get good at something? How do you achieve mastery?

Knowing what you're doing is important. You can read 100 books about how to play the violin. There are violin methods going back hundreds of years. While you're at it, why not read one of the many books on how to build a violin. That will help you soak up the essence of the violin.

If you read a couple dozen violin books on building and playing them, you'll know a lot about the violin. How they're made, how they're tuned, how they're played.

But eventually you need to get your hands on a fiddle, put it under your chin and start sawing away with a horsehair bow. The instruction of a good teacher will help. It's probably mandatory.

Then you need to get good.

How do you do that? There's really only one way:

You do that thing.

Many, many, many times. Every day. For as long as it takes. Understanding and knowledge are important, but doing well means in large part doing often.

We are told mastery is good. How do you arrive at mastery?

In Western culture, it's better when that mastery is the result more of pure genius and less relentless, repetitious toil. It makes for a better story.

But even geniuses need to practice.

When I saw violinist Joshua Bell lead the Academy of St. Martin in the Fields as both conductor and concertmaster, playing the violin with speed, fluidity and ease, it looks like the most natural thing in the world. It seems effortless.

Yet he didn't get there without many thousands of hours of practice and performance over decades.

Scales, exercises, etudes, studies. And more of the same. Every day. Year after year.

Without constant repetition, Joshua Bell the virtuoso could never happen.

I don't necessarily want to compare mastering the violin with washing dishes, sweeping the floor, and setting up and maintaining a Linux system.

But I could do that. And I will.

The secret to mastery is embracing the toil, the repetition, the practice.

Running \texttt{sudo apt update} and then \texttt{sudo apt upgrade} won't get you to Carnegie Hall (except maybe to the venue's IT department).

But you'll have a fully patched Debian system that's ready to run.

And keeping the system up to date goes the same way most times:

\begin{verbatim}
steven@steven-debian:~$ sudo apt update
[sudo] password for steven: 
Hit:1 http://deb.debian.org/debian buster InRelease
Hit:2 http://security.debian.org/debian-security buster/updates 
InRelease
Hit:3 http://deb.debian.org/debian buster-updates InRelease
Hit:4 http://deb.debian.org/debian buster-backports InRelease
Reading package lists... Done
Building dependency tree       
Reading state information... Done
All packages are up to date.
steven@steven-debian:~$
\end{verbatim}

If there were any packages that needed updating, the next command would be:

\begin{verbatim}
steven@steven-debian:~$ sudo apt upgrade
\end{verbatim}

And that's about it during the life of a Debian Stable release.

On my current Debian laptop, I do this check every day, usually in the morning. There are never too many updates because it's Debian \textit{Stable}. I could run an update/upgrade once a week, or even as seldom as once a month.

Weekly is better. I like to think of running an update/upgrade daily as \textit{something you do}.

Many days there are no updates. Sometimes you have two or three. On a day when Firefox updates, there are so many language packages that you could have 50.

Keep an eye on \url{http://debian.org/security}. There you can see all of the updates the Debian Security Team has sent through the repositories to keep your system running as safely as possible. Sometimes there are even bug fixes, though the whole point of Debian Stable (that being stability) means that security patches are king and software "improvements" wait for the next release.

\chapter{The command line}
As Neal Stephenson famously wrote, \textit{In the Beginning Was the Command Line}\footnote{"In the Beginning Was the Command Line," Neal Stephenson \url{https://smorgasborg.artlung.com/C_R_Y_P_T_O_N_O_M_I_C_O_N.shtml} (Also available as a print or ebook)

"In the Beginning Was the Command Line," Neal Stephenson with 2004 update by Garrett Birkel \url{http://garote.bdmonkeys.net/commandline/index.html}}.

To make a long story much shorter, before graphical user interfaces with mice-controlled pointers, computers booted up with a prompt and patiently waited for you to type something.

That interface is still alive today. Almost all servers contain nothing but. Your Linux desktop system also has a command line. Many of the same commands you might have typed into a Unix system in the 1970s and '80s will work today.

There is still tremendous utility in the command line. Some call it the console, the terminal, or the Unix prompt. They all describe the same thing.

A significant percentage of software developers prefer working with the command line and its thousands of text-based tools. For a lot of development, you really have no choice.

MacOS has a command line. So does Windows. The Mac's, based on the early Unix variant called BSD, is much like the command line at the center of Linux. Apple does everything it can to make sure the users of MacOS never have to open a terminal. There are GUI boxes and menus for everything you would ever need.

Yet the command line persists — and thrives — in MacOS. The Macbook Pro is the laptop of choice for legions of developers, even though Linux on just about any other computer offers most of the same programming tools, and many more. Of those, the Linux versions can be better and faster. I'm looking at YOU, Homebrew, the Linux-like package manager for Mac that couldn't be slower and which made me long for Debian's APT and even Fedora's DNF.

With Debian — and any Linux or BSD Unix system, you know what's in the tin, as the British say. (We Americans call it a "can," but the expression references a tin, so I'll stick with it.)

\section*{The Windows terminal, like MS-DOS (sort of)}

Before Windows — and definitely before Windows 95, there was MS-DOS, the Microsoft command-line system that many an office worker saw before they fired up Wordstar, Visicalc, dBase, Lotus 1-2-3 and even early versions of Microsoft Word. (I remember running MS Word on DOS 3.3 and 5.)

Like Apple, Microsoft would prefer that its users never touch the Windows command line, which can be summoned by searching for CMD. But now that the basic Windows CMD application has been joined by IT department favorite PowerShell, a new Windows Terminal, and even a full Linux console in the form of the Windows Subsystem for Linux, the command line is getting way more attention in Microsoft's OS than on Apple's Macintosh.

But why try to replicate what Linux can already offer you: A real Linux kernel running your system and available for almost any computing task.

I spent a couple years running Windows 10 with an Ubuntu system via the WSL. I couldn't manage any part of the Windows system with the Ubuntu shell, and sharing files between the WSL and "proper" Windows was inconvenient at best. Microsoft is actively improving the WSL. (They REALLY want developers to use Windows.)

After a couple of extremely troubling Windows upgrades that cost me days of productivity at a time, I thought, "Why am I fighting this?" I don't need Adobe or Microsoft applications. I got an NVMe M.2 SSD drive, installed Debian Stable on it and never looked back.

\chapter{Debian is people}

Debian lives in its mailing lists. Subscribe to a bunch just to get the flavor.

I have a Google account that I pretty much dedicate to to mailing list, and I filter all of the individual Debian lists to dedicated tags

Planet Debian

\part*{Ending matter}

\chapter{References}

"In the Beginning Was the Command Line," Neal Stephenson \url{https://smorgasborg.artlung.com/C_R_Y_P_T_O_N_O_M_I_C_O_N.shtml} (Also available as a print or ebook, \url{https://www.amazon.com/Beginning-was-Command-Line/dp/0380815931})

"In the Beginning Was the Command Line," Neal Stephenson with 2004 update by Garrett Birkel \url{http://garote.bdmonkeys.net/commandline/index.html}
\chapter{License}
Copyright © 2023 Steven Rosenberg

\bigskip Made available under the following Creative Commons license:

\bigskip \textbf{Attribution-NonCommercial-NoDerivatives 4.0 International}

\bigskip Creative Commons Corporation ("Creative Commons") is not a law firm and
does not provide legal services or legal advice. Distribution of
Creative Commons public licenses does not create a lawyer-client or
other relationship. Creative Commons makes its licenses and related
information available on an "as-is" basis. Creative Commons gives no
warranties regarding its licenses, any material licensed under their
terms and conditions, or any related information. Creative Commons
disclaims all liability for damages resulting from their use to the
fullest extent possible.

\bigskip \textbf{Using Creative Commons Public Licenses}

\bigskip Creative Commons public licenses provide a standard set of terms and
conditions that creators and other rights holders may use to share
original works of authorship and other material subject to copyright
and certain other rights specified in the public license below. The
following considerations are for informational purposes only, are not
exhaustive, and do not form part of our licenses.

     Considerations for licensors: Our public licenses are
     intended for use by those authorized to give the public
     permission to use material in ways otherwise restricted by
     copyright and certain other rights. Our licenses are
     irrevocable. Licensors should read and understand the terms
     and conditions of the license they choose before applying it.
     Licensors should also secure all rights necessary before
     applying our licenses so that the public can reuse the
     material as expected. Licensors should clearly mark any
     material not subject to the license. This includes other CC-
     licensed material, or material used under an exception or
     limitation to copyright. More considerations for licensors:
    \url{wiki.creativecommons.org/Considerations_for_licensors}

     Considerations for the public: By using one of our public
     licenses, a licensor grants the public permission to use the
     licensed material under specified terms and conditions. If
     the licensor's permission is not necessary for any reason--for
     example, because of any applicable exception or limitation to
     copyright--then that use is not regulated by the license. Our
     licenses grant only permissions under copyright and certain
     other rights that a licensor has authority to grant. Use of
     the licensed material may still be restricted for other
     reasons, including because others have copyright or other
     rights in the material. A licensor may make special requests,
     such as asking that all changes be marked or described.
     Although not required by our licenses, you are encouraged to
     respect those requests where reasonable. More considerations
     for the public:
    \url{wiki.creativecommons.org/Considerations_for_licensees}

\noindent\rule{\textwidth}{1pt}

Creative Commons Attribution-NonCommercial-NoDerivatives 4.0
International Public License

By exercising the Licensed Rights (defined below), You accept and agree
to be bound by the terms and conditions of this Creative Commons
Attribution-NonCommercial-NoDerivatives 4.0 International Public
License ("Public License"). To the extent this Public License may be
interpreted as a contract, You are granted the Licensed Rights in
consideration of Your acceptance of these terms and conditions, and the
Licensor grants You such rights in consideration of benefits the
Licensor receives from making the Licensed Material available under
these terms and conditions.


Section 1 -- Definitions.

  a. Adapted Material means material subject to Copyright and Similar
     Rights that is derived from or based upon the Licensed Material
     and in which the Licensed Material is translated, altered,
     arranged, transformed, or otherwise modified in a manner requiring
     permission under the Copyright and Similar Rights held by the
     Licensor. For purposes of this Public License, where the Licensed
     Material is a musical work, performance, or sound recording,
     Adapted Material is always produced where the Licensed Material is
     synched in timed relation with a moving image.

  b. Copyright and Similar Rights means copyright and/or similar rights
     closely related to copyright including, without limitation,
     performance, broadcast, sound recording, and Sui Generis Database
     Rights, without regard to how the rights are labeled or
     categorized. For purposes of this Public License, the rights
     specified in Section 2(b)(1)-(2) are not Copyright and Similar
     Rights.

  c. Effective Technological Measures means those measures that, in the
     absence of proper authority, may not be circumvented under laws
     fulfilling obligations under Article 11 of the WIPO Copyright
     Treaty adopted on December 20, 1996, and/or similar international
     agreements.

  d. Exceptions and Limitations means fair use, fair dealing, and/or
     any other exception or limitation to Copyright and Similar Rights
     that applies to Your use of the Licensed Material.

  e. Licensed Material means the artistic or literary work, database,
     or other material to which the Licensor applied this Public
     License.

  f. Licensed Rights means the rights granted to You subject to the
     terms and conditions of this Public License, which are limited to
     all Copyright and Similar Rights that apply to Your use of the
     Licensed Material and that the Licensor has authority to license.

  g. Licensor means the individual(s) or entity(ies) granting rights
     under this Public License.

  h. NonCommercial means not primarily intended for or directed towards
     commercial advantage or monetary compensation. For purposes of
     this Public License, the exchange of the Licensed Material for
     other material subject to Copyright and Similar Rights by digital
     file-sharing or similar means is NonCommercial provided there is
     no payment of monetary compensation in connection with the
     exchange.

  i. Share means to provide material to the public by any means or
     process that requires permission under the Licensed Rights, such
     as reproduction, public display, public performance, distribution,
     dissemination, communication, or importation, and to make material
     available to the public including in ways that members of the
     public may access the material from a place and at a time
     individually chosen by them.

  j. Sui Generis Database Rights means rights other than copyright
     resulting from Directive 96/9/EC of the European Parliament and of
     the Council of 11 March 1996 on the legal protection of databases,
     as amended and/or succeeded, as well as other essentially
     equivalent rights anywhere in the world.

  k. You means the individual or entity exercising the Licensed Rights
     under this Public License. Your has a corresponding meaning.


Section 2 -- Scope.

  a. License grant.

       1. Subject to the terms and conditions of this Public License,
          the Licensor hereby grants You a worldwide, royalty-free,
          non-sublicensable, non-exclusive, irrevocable license to
          exercise the Licensed Rights in the Licensed Material to:

            a. reproduce and Share the Licensed Material, in whole or
               in part, for NonCommercial purposes only; and

            b. produce and reproduce, but not Share, Adapted Material
               for NonCommercial purposes only.

       2. Exceptions and Limitations. For the avoidance of doubt, where
          Exceptions and Limitations apply to Your use, this Public
          License does not apply, and You do not need to comply with
          its terms and conditions.

       3. Term. The term of this Public License is specified in Section
          6(a).

       4. Media and formats; technical modifications allowed. The
          Licensor authorizes You to exercise the Licensed Rights in
          all media and formats whether now known or hereafter created,
          and to make technical modifications necessary to do so. The
          Licensor waives and/or agrees not to assert any right or
          authority to forbid You from making technical modifications
          necessary to exercise the Licensed Rights, including
          technical modifications necessary to circumvent Effective
          Technological Measures. For purposes of this Public License,
          simply making modifications authorized by this Section 2(a)
          (4) never produces Adapted Material.

       5. Downstream recipients.

            a. Offer from the Licensor -- Licensed Material. Every
               recipient of the Licensed Material automatically
               receives an offer from the Licensor to exercise the
               Licensed Rights under the terms and conditions of this
               Public License.

            b. No downstream restrictions. You may not offer or impose
               any additional or different terms or conditions on, or
               apply any Effective Technological Measures to, the
               Licensed Material if doing so restricts exercise of the
               Licensed Rights by any recipient of the Licensed
               Material.

       6. No endorsement. Nothing in this Public License constitutes or
          may be construed as permission to assert or imply that You
          are, or that Your use of the Licensed Material is, connected
          with, or sponsored, endorsed, or granted official status by,
          the Licensor or others designated to receive attribution as
          provided in Section 3(a)(1)(A)(i).

  b. Other rights.

       1. Moral rights, such as the right of integrity, are not
          licensed under this Public License, nor are publicity,
          privacy, and/or other similar personality rights; however, to
          the extent possible, the Licensor waives and/or agrees not to
          assert any such rights held by the Licensor to the limited
          extent necessary to allow You to exercise the Licensed
          Rights, but not otherwise.

       2. Patent and trademark rights are not licensed under this
          Public License.

       3. To the extent possible, the Licensor waives any right to
          collect royalties from You for the exercise of the Licensed
          Rights, whether directly or through a collecting society
          under any voluntary or waivable statutory or compulsory
          licensing scheme. In all other cases the Licensor expressly
          reserves any right to collect such royalties, including when
          the Licensed Material is used other than for NonCommercial
          purposes.


Section 3 -- License Conditions.

Your exercise of the Licensed Rights is expressly made subject to the
following conditions.

  a. Attribution.

       1. If You Share the Licensed Material, You must:

            a. retain the following if it is supplied by the Licensor
               with the Licensed Material:

                 i. identification of the creator(s) of the Licensed
                    Material and any others designated to receive
                    attribution, in any reasonable manner requested by
                    the Licensor (including by pseudonym if
                    designated);

                ii. a copyright notice;

               iii. a notice that refers to this Public License;

                iv. a notice that refers to the disclaimer of
                    warranties;

                 v. a URI or hyperlink to the Licensed Material to the
                    extent reasonably practicable;

            b. indicate if You modified the Licensed Material and
               retain an indication of any previous modifications; and

            c. indicate the Licensed Material is licensed under this
               Public License, and include the text of, or the URI or
               hyperlink to, this Public License.

          For the avoidance of doubt, You do not have permission under
          this Public License to Share Adapted Material.

       2. You may satisfy the conditions in Section 3(a)(1) in any
          reasonable manner based on the medium, means, and context in
          which You Share the Licensed Material. For example, it may be
          reasonable to satisfy the conditions by providing a URI or
          hyperlink to a resource that includes the required
          information.

       3. If requested by the Licensor, You must remove any of the
          information required by Section 3(a)(1)(A) to the extent
          reasonably practicable.


Section 4 -- Sui Generis Database Rights.

Where the Licensed Rights include Sui Generis Database Rights that
apply to Your use of the Licensed Material:

  a. for the avoidance of doubt, Section 2(a)(1) grants You the right
     to extract, reuse, reproduce, and Share all or a substantial
     portion of the contents of the database for NonCommercial purposes
     only and provided You do not Share Adapted Material;

  b. if You include all or a substantial portion of the database
     contents in a database in which You have Sui Generis Database
     Rights, then the database in which You have Sui Generis Database
     Rights (but not its individual contents) is Adapted Material; and

  c. You must comply with the conditions in Section 3(a) if You Share
     all or a substantial portion of the contents of the database.

For the avoidance of doubt, this Section 4 supplements and does not
replace Your obligations under this Public License where the Licensed
Rights include other Copyright and Similar Rights.


Section 5 -- Disclaimer of Warranties and Limitation of Liability.

  a. UNLESS OTHERWISE SEPARATELY UNDERTAKEN BY THE LICENSOR, TO THE
     EXTENT POSSIBLE, THE LICENSOR OFFERS THE LICENSED MATERIAL AS-IS
     AND AS-AVAILABLE, AND MAKES NO REPRESENTATIONS OR WARRANTIES OF
     ANY KIND CONCERNING THE LICENSED MATERIAL, WHETHER EXPRESS,
     IMPLIED, STATUTORY, OR OTHER. THIS INCLUDES, WITHOUT LIMITATION,
     WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR
     PURPOSE, NON-INFRINGEMENT, ABSENCE OF LATENT OR OTHER DEFECTS,
     ACCURACY, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT
     KNOWN OR DISCOVERABLE. WHERE DISCLAIMERS OF WARRANTIES ARE NOT
     ALLOWED IN FULL OR IN PART, THIS DISCLAIMER MAY NOT APPLY TO YOU.

  b. TO THE EXTENT POSSIBLE, IN NO EVENT WILL THE LICENSOR BE LIABLE
     TO YOU ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION,
     NEGLIGENCE) OR OTHERWISE FOR ANY DIRECT, SPECIAL, INDIRECT,
     INCIDENTAL, CONSEQUENTIAL, PUNITIVE, EXEMPLARY, OR OTHER LOSSES,
     COSTS, EXPENSES, OR DAMAGES ARISING OUT OF THIS PUBLIC LICENSE OR
     USE OF THE LICENSED MATERIAL, EVEN IF THE LICENSOR HAS BEEN
     ADVISED OF THE POSSIBILITY OF SUCH LOSSES, COSTS, EXPENSES, OR
     DAMAGES. WHERE A LIMITATION OF LIABILITY IS NOT ALLOWED IN FULL OR
     IN PART, THIS LIMITATION MAY NOT APPLY TO YOU.

  c. The disclaimer of warranties and limitation of liability provided
     above shall be interpreted in a manner that, to the extent
     possible, most closely approximates an absolute disclaimer and
     waiver of all liability.


Section 6 -- Term and Termination.

  a. This Public License applies for the term of the Copyright and
     Similar Rights licensed here. However, if You fail to comply with
     this Public License, then Your rights under this Public License
     terminate automatically.

  b. Where Your right to use the Licensed Material has terminated under
     Section 6(a), it reinstates:

       1. automatically as of the date the violation is cured, provided
          it is cured within 30 days of Your discovery of the
          violation; or

       2. upon express reinstatement by the Licensor.

     For the avoidance of doubt, this Section 6(b) does not affect any
     right the Licensor may have to seek remedies for Your violations
     of this Public License.

  c. For the avoidance of doubt, the Licensor may also offer the
     Licensed Material under separate terms or conditions or stop
     distributing the Licensed Material at any time; however, doing so
     will not terminate this Public License.

  d. Sections 1, 5, 6, 7, and 8 survive termination of this Public
     License.


Section 7 -- Other Terms and Conditions.

  a. The Licensor shall not be bound by any additional or different
     terms or conditions communicated by You unless expressly agreed.

  b. Any arrangements, understandings, or agreements regarding the
     Licensed Material not stated herein are separate from and
     independent of the terms and conditions of this Public License.


Section 8 -- Interpretation.

  a. For the avoidance of doubt, this Public License does not, and
     shall not be interpreted to, reduce, limit, restrict, or impose
     conditions on any use of the Licensed Material that could lawfully
     be made without permission under this Public License.

  b. To the extent possible, if any provision of this Public License is
     deemed unenforceable, it shall be automatically reformed to the
     minimum extent necessary to make it enforceable. If the provision
     cannot be reformed, it shall be severed from this Public License
     without affecting the enforceability of the remaining terms and
     conditions.

  c. No term or condition of this Public License will be waived and no
     failure to comply consented to unless expressly agreed to by the
     Licensor.

  d. Nothing in this Public License constitutes or may be interpreted
     as a limitation upon, or waiver of, any privileges and immunities
     that apply to the Licensor or You, including from the legal
     processes of any jurisdiction or authority.

\noindent\rule{\textwidth}{1pt}

Creative Commons is not a party to its public
licenses. Notwithstanding, Creative Commons may elect to apply one of
its public licenses to material it publishes and in those instances
will be considered the “Licensor.” The text of the Creative Commons
public licenses is dedicated to the public domain under the CC0 Public
Domain Dedication. Except for the limited purpose of indicating that
material is shared under a Creative Commons public license or as
otherwise permitted by the Creative Commons policies published at
creativecommons.org/policies, Creative Commons does not authorize the
use of the trademark "Creative Commons" or any other trademark or logo
of Creative Commons without its prior written consent including,
without limitation, in connection with any unauthorized modifications
to any of its public licenses or any other arrangements,
understandings, or agreements concerning use of licensed material. For
the avoidance of doubt, this paragraph does not form part of the
public licenses.

Creative Commons may be contacted at \url{creativecommons.org}.
\end{document}

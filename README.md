# The Zen of Debian

"The Zen of Debian" is an essay about Debian GNU/Linux. It's also a personal account of my computing journey and how an operating system like Debian — or really any Linux- or BSD-based OS — can bring order, calm, productivity and stability to your life and work.

## Get the PDF

Since July 4, 2022, I have been working on this project in LaTex, mostly using Texmaker, and the original markdown version — frozen in time — will live in its own branch.

If all you want to do is download and read the PDF, you can [get it here](https://passthejoe.net/zen/zen.pdf).

The PDF is no longer under version control, but the source is. That's what's in this repo. I hope to upload new PDFs as frequently as needed to keep up with changes in the source.

## Who am I?

My name is [Steven Rosenberg](http://passthejoe.net). I am a journalist, writer and developer. Over the last decade and then some, I have written quite a bit about my experiences with computer hardware and software as a pretty much regular user learning things along the way. Much of that writing can be found at <http://blogs.dailynews.com/click> (mirrored at <https://passthejoe.wordpress.com>) and <http://stevenrosenberg.net/blog>. Newer posts are at <http://passthejoe.net>.

During years and years of blogging, I have written hundreds of posts about Debian, Ubuntu, Fedora and other Linux distributions. I haven't counted, but I probably wrote more than 100 posts about OpenBSD.

I'm sure I've written about the drought in Linux book publishing, and I know that few are writing — and even fewer are publishing — books about how to use Linux on the desktop. There's certainly no money in it.

I began using Linux and BSD operating systems around 2007 when I didn't have a computer of my own and was hacking on old machines. I had a surplus thin client with a VIA CPU and IDE hard and CD drives hanging out the back. I bought a very, very used Compaq laptop for $15. Then I got a broken Gateway laptop for free. I fixed it and ran it for years. Even when these castoff computers had a Windows OS, I always ran Linux or BSD on them. The first distro I _really_ used was probably Puppy Linux. 

During those early Linux days, I found out about Ubuntu during the 6.06 and 7.04 era and had heard how it was an "easier" Debian. (First question: What's Debian, and why is it so hard?)

When Debian Etch came out, I read about it, downloaded an ISO and gave it a spin. The installer wasn't much more complicated than Ubuntu's, and once I had Debian running, it looked, smelled and worked like a bluer, faster Ubuntu. At that time, Debian, Ubuntu, SuSE, Fedora and Red Hat Enterprise Linux/CentOS all shipped with a very similar GNOME 2 desktop. Package management was different (yum vs apt and all), but it soon became very apparent that if you could run Ubuntu, you could run Debian.

In more recent years — 2012 and 2017 — I have used Windows on new laptops in order to give the OS "a chance" and avoid compatibility issues that can affect new hardware. I gave Windows 10 maybe two years on my current HP Envy laptop before giving up after some very thorny upgrades and enough quirks to make me question why I didn't just return to an OS where simplicity, stability and performance are all part of the package (and package management system).

For me over the years, those OSes have been the Debian and Fedora distributions of Linux. I first used Debian in the 4.0 "Etch" era, and was surprise to find out it was a bluer, faster yet not appreciably more difficult version of what the brown yet growing Ubuntu was offering in the 6.06 era. I experimented rather heavily with both Debian and Ubuntu at the time, and I seemed to always come back to Debian.

I looked back at the [list of Debian releases](https://wiki.debian.org/DebianReleases) and realize that I've spent time using every one from Etch (Version 4) through the current stable release Buster (Version 10).

I have run _a lot_ of [Fedora over the years](https://en.wikipedia.org/wiki/Fedora_version_history). I began 2010 with Version 13 and stuck with Fedora through version 15, turning to Debian Squeeze when a Fedora upgrade went really bad due to the proprietary AMD/Catalyst video driver. I returned to Fedora with my next laptop in 2012 — the Fedora 17 "Beefy Miracle" era. I still have that laptop today, and it runs Fedora 30, I had ONE bad upgrade in all those years, and that time I was able to reinstall Fedora and use the same partitions and `/home` directory.  

With my current laptop, a 2017 HP Envy, I decided to spend some time using Windows 10, and it was a couple of years before thorny upgrades, persistent bugs and lagging performance led me to get a small, cheap M.2 NVMe SSD, and install Debian Stable (version 10 Buster). I immediately began to feel comfortable and productive on the computer I use for wrangling websites, writing and editing, hacking on Ruby, and all of the other day-to-day things I do with a PC.  

**Aug. 8, 2020:** Adding chapters: The Zen in Debian, The command line

**June 14, 2020:** I agree with [the movement that says](https://www.zdnet.com/article/github-to-replace-master-with-alternative-term-to-avoid-slavery-references/) the technology terms "master/slave" should go away, and the primary Git (and GitHub and Codeberg) repository in this project is now called `main` instead of `master`.

**June 8, 2022:** It's been a long time. Now I'm thinking that I want to format this in LaTeX and output it as a PDF. I'm looking into that.

**July 4, 2022:** Today I am "moving" the project from Github to [Codeberg](https://codeberg.org/). A book about one of the leading free software projects should be hosted on a free software platform, and Codeberg is based on [Gitea](https://gitea.io).

I am also beginning to experiment with LaTex, and I will start a branch for that version, which will become the main pretty soon because it makes sence to write in that document.

**Sept. 4, 2022:** I am now working on this project in Texmaker on Debian 11 Bullseye on my 2017 HP Envy laptop. Before this I had been using Fedora Silverblue with the Texmaker Flatpak. I still had my Debian Buster desktop, but I don't have it set up for LaTeX yet. 

**May 4, 2023:** My laptop has been upgraded to Debian 12 Bookworm. The desktop (2011 iMac) still runs Debian 11 for now. In this README, I eliminated references and links to separate chapter files, which no longer exist now that the "book" is one big .tex file.
